import React from 'react';
import Search from './Search';

function App() {
  return (
    <div className="content">
      <Search/>
    </div>
  );
}

export default App;
