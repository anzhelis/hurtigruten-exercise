import React, { useCallback, useState } from 'react';
import './Search.css';
import crossIcon from './assets/cross.svg';
import _ from "lodash";

function Search() {
    const [userInput, setUserInput] = useState("");
    const [ships, setShips] = useState([])
    const delayQuery = useCallback(
        _.throttle((query) => getShips(query), 800),
        []
    );

    const handleChange = event => {
        const newValue = event.target.value;
        setUserInput(newValue);
        if (newValue.length > 1) {
            delayQuery(newValue)
            .then(res => setShips(res))
            .catch(err => console.log(err))
        }
    }

    const getShips = async (query) => {
        const response = await fetch(`/api/ships/${query}`);
        const body = await response.json();
        return body;
    }

    return (
        <div className="container">
            <div className="search-input">
            <input 
                type="text" 
                className="user-input" 
                name="search"  
                placeholder="Search" 
                value={userInput} 
                onChange={handleChange}
                />
            <img 
            className="cross-icon" 
            alt="clear the input"
            src={crossIcon} 
            onClick={()=> setUserInput("")}
            />
            </div>
        {userInput.length >1 && 
            ships.map( ship => {
                return <div 
                key={ship.id} 
                className="suggestion" 
                onClick={()=>window.open("https://www."+ship.url)}>
                    {ship.name}
                </div>
        })}
        </div>
    );
}

export default Search;